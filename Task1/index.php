<?php
 include('velebi.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Task 1</title>
</head>
<body>
    <!-- <h1>სარეგისტრაციო ფორმა</h1>
    <br><br>
    <h2>ელპოსტა: </h2><input type="text" name="email"> 
    <h2>პაროლი:</h2><input type="text" name="password">
    <br><br>
    <h2>გაიმეორეთ პაროლი:</h2><input type="text" name="repeatpassword">
    <br><br>
    <input type="submit" name="submit" value="Submit"> 
    </form> -->

    <hr>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
    <label for="email">ელპოსტა:</label>
    <input type="text" id="email" name="email" value="<?php echo $email; ?>" required>
    <span style="color: red;"><?php echo $emailError; ?></span><br><br>

    <label for="password">პაროლი:</label>
    <input type="password" name="password" required><br><br>

    <label for="repeat_password">გაიმეორეთ პაროლი:</label>
    <input type="password" name="repeat_password" required><br><br>

    <input type="submit" value="Submit">
</form>
</body>
</html>